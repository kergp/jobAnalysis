# encoding: utf-8
import scrapy


class FundItem(scrapy.Item):
    # define the fields for your item here like:
    # name = scrapy.Field()
    fname_fund_code = scrapy.Field()
    issue_date = scrapy.Field()
    per_net = scrapy.Field()
    total_net = scrapy.Field()
    growth_rate = scrapy.Field()

    def printProps(self):
        #[发布时间] [单位净值] [累计净值] [日增长率]

        return " %s, %s, %s, %s, %s \n" % (self['fname_fund_code'], self['issue_date'], self['per_net'], self['total_net'],self['growth_rate'])
    def getFname(self):
        return "%s" % (self['fname_fund_code'])