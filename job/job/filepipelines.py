# -*- coding: utf-8 -*-

# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: http://doc.scrapy.org/en/latest/topics/item-pipeline.html
import os

class JobPipeline(object):
    def process_item(self, item, spider):
        fname = item.getFname()
        f_path = r"./fund_data/"+fname+".txt"
        dir_name = os.path.dirname(f_path)
        if not os.path.exists(dir_name):
            os.makedirs(dir_name)
        f_handle = open(f_path,'a')
        # print(item)
        print(spider.name)
        f_handle.write(item.printProps())
        f_handle.close()
        return item